<?php

$scriptUrl = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'] . '/cocal.php';
$publicKeyPath = 'file://path/to/key.pub';

$matrNr = $_POST["matr"];
$password = $_POST["pass"];

$summary = '${[buildingNr|roomNr (roomName)] }${name}'; // DEFAULT
if (!empty($_POST['summary'])) {
	$summary = $_POST['summary'];
}

$combined = $summary . "\r\n" . $matrNr . "\r\n" . $password;
$publicKey = openssl_pkey_get_public($publicKeyPath);

openssl_public_encrypt($combined, $cipher, $publicKey);

$encodedCipher = rawurlencode($cipher);

$fullLink = $scriptUrl . "?code=" . $encodedCipher;

if (!empty($_POST['showCategory'])) {
	$fullLink .= "&showCategory=1";
}
if (!empty($_POST['showRoom'])) {
	$fullLink .= "&showRoom=1";
}

?>

<!doctype html>
<html lang="de">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <title>CampusOffice-Kalender Sync - Link</title>
  </head>
  <body class="bg-light">
    <div class="container">
      <h1>CampusOffice-Kalender Sync - Link</h1>
	  <div class="alert alert-primary" role="alert">
	    <a href="<?php echo $fullLink;?>" class="alert-link">Kalendar-Link</a>
	  </div>

	  <a href="index.html" role="button" class="btn btn-primary btn-lg btn-block">Zurück</a>

    </div>


    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  </body>
</html>