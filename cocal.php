<?php

mb_internal_encoding("UTF-8");

/* Constants */
$scriptUrl = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
$baseUrl = 'https://www.campus.rwth-aachen.de/';
$homePath = 'office/default.asp';
$loginPath = 'office/views/campus/redirect.asp';
$calPath = 'office/views/calendar/iCalExport.asp';
$logoutPath = 'office/system/login/logoff.asp';
$roomPath = 'rwth/all/room.asp';
$privateKeyPath  = 'file://path/to/key.private';
$roomDatabasePath = '/path/to/cocal.db';

/* Functions */
function curl_fixcookie($cookieFile) {
	$contents = file_get_contents($cookieFile);
	$lines = explode("\n", $contents);

	foreach ($lines as $i => $line) {
		if (strpos($line, "#HttpOnly_") === 0) {
			$lines[$i] = substr($line, strlen("#HttpOnly_"));
		}
	}

	$contents = implode("\n", $lines);
	file_put_contents($cookieFile, $contents);
}

function curl_request($method, $url, $cookieFile = false, $params = array()) {
	$ch = curl_init();

	$options = array(
		CURLOPT_URL		=> $url,
		CURLOPT_FOLLOWLOCATION	=> true,
		CURLOPT_RETURNTRANSFER	=> true,
		CURLOPT_HEADER		=> true
	);

	if ($cookieFile) {
		$options[CURLOPT_COOKIEFILE] = $cookieFile;
		$options[CURLOPT_COOKIEJAR] = $cookieFile;
	}

	array_walk($params, function(&$value, $key) { $value = $key . '=' . $value; });

	if ($params && strtolower($method) == 'post') {
		$options[CURLOPT_POST] = true;
		$options[CURLOPT_POSTFIELDS] = implode("&", $params);
	}
	else if ($params) { /* assuming default mehtod: GET */
		$options[CURLOPT_URL] .= '?' . implode('&', $params);
	}

	curl_setopt_array($ch, $options);
	$output = curl_exec($ch);
	curl_close($ch);

	if ($cookieFile) {
		curl_fixcookie($cookieFile);
	}

	return $output;
}

function get_address($db, $room) {
	return $db->querySingle('SELECT * FROM rooms WHERE id = "' . $db->escapeString($room). '";', true);
}

function set_address($db, $room, $address) {
	$db->exec('INSERT OR REPLACE INTO rooms VALUES (
			"' . $db->escapeString($room) . '",
			"' . $db->escapeString(@$address['address']) . '",
			"' . $db->escapeString(@$address['cluster']) . '",
			"' . $db->escapeString(@$address['building']) . '",
			"' . $db->escapeString(@$address['building_no']) . '",
			"' . $db->escapeString(@$address['room']) . '",
			"' . $db->escapeString(@$address['room_no']) . '",
			"' . $db->escapeString(@$address['floor']) . '"
		);');
}

function crawl_address($room) {
	global $baseUrl, $roomPath;

	$matches = array();
	$response = curl_request('GET', $baseUrl . $roomPath . '?room=' . urlencode($room));

	$infos = array(
		'cluster' => 'H.rsaalgruppe',
		'address' => 'Geb.udeanschrift',
		'building' => 'Geb.udebezeichung',
		'building_no' => 'Geb.udenummer',
		'room' => 'Raumname',
		'room_no' => 'Raumnummer',
		'floor' => 'Geschoss'
	);

	foreach ($infos as $index => $pattern) {
		$match = array();

		if (preg_match('/<td class="default">' . $pattern . '<\/td><td class="default">([^<]*)<\/td>/', $response, $match)) {
			$matches[$index] = preg_replace('/[ ]{2,}/sm', ' ', utf8_encode($match[1]));
		}
	}

	return (count($matches)) ? $matches : false;
}

/* send HTTP 500 for Google to stop fetching */
function error() {
	global $scriptUrl;

	header("HTTP/1.0 500 Internal Server Error");
	echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
		<html>
			<head>
				<meta http-equiv="content-type" content="text/html; charset=UTF-8">
			</head>
			<body>
				<div id="content"><h2>Sorry an error occured!<br />Check your credentials and try again!</h2></div>
			</body>
		</html>';
	die();
}

function parseSummary($summary, $event) {
	$value = "";

	$len = strlen($summary);
	$summary = preg_split('//u', $summary, null, PREG_SPLIT_NO_EMPTY);
	
	$summary[$len] = '\n'; // Extend by one character to make parsing easier when looking one character ahead
	
	for ($pos = 0; $pos < $len; $pos++) {
		// Format is ${content}
		if ($summary[$pos] == "$" && $summary[$pos + 1] == "{") {
			$pos += 2;
			$value .= parseBracketPart($event, $summary, $pos, $len, "{", "}", false);
		}
		else  {
			$value .= $summary[$pos];
		}
	}
	return $value;
}

function parseBracketPart($event, $summary, &$pos, $len, $beginChar, $endChar, $printBracket) {
	$value = "";
	for (;$pos < $len; $pos++) {
		if ($summary[$pos] == $endChar) {
			break;
		}
		if (ctype_alpha($summary[$pos])) {
			$var = acceptVariable($summary, $pos, $len);
			$value .= parseVariable($event, $var);
		}
		else if ($summary[$pos] == "[") {
			$pos++;
			$value .= parseBracketPart($event, $summary, $pos, $len, "[", "]", true);
		}
		else if ($summary[$pos] == "{") {
			$pos++;
			$value .= parseBracketPart($event, $summary, $pos, $len, "{", "}", true);
		}
		else if ($summary[$pos] == "(") {
			$pos++;
			$value .= parseBracketPart($event, $summary, $pos, $len, "(", ")", true);
		}
		else if ($summary[$pos] == "|") {
			$pos++;

			$afterPipe = parseBracketPart($event, $summary, $pos, $len, $beginChar, $endChar, false);
			if (!empty(trim($value)) || !empty(trim($afterPipe))) {
				$value .= "|" . $afterPipe;
			}
			break;

		}
		else {
			$value .= $summary[$pos];
		}
	}

	if ($printBracket && !empty(trim($value))) {
		$value = $beginChar . $value . $endChar;
	}

	return $value;
}

function parseVariable($event, $name) {
	$address = $event['address'];
	switch ($name) {
		case "roomNr": return $address['room_no'] ?? "";
		case "roomName": return $address['room'] ?? "";
		case "buildingNr": return $address['building_no'] ?? "";
		case "buildingName": return $address['building'] ?? "";
		case "category": return $event['CATEGORIES'] ?? "";
		case "name": return $event['SUMMARY'] ?? "";
	}
	return "";
}

function acceptVariable($summary, &$pos, $len) {
	$name = "";
	for (;$pos < $len; $pos++) {
		if (!ctype_alpha($summary[$pos])) {
			$pos--;
			break;
		}
		$name .= $summary[$pos];
	}
	return $name;
}

/* Code */
if (!empty($_GET['code'])) {
	$privateKey = openssl_pkey_get_private($privateKeyPath);
	openssl_private_decrypt(rawurldecode($_GET['code']), $cipher, $privateKey);
	
	if (strpos($cipher, "\r\n")) {
		list($summary, $matrnr, $passwd) = explode("\r\n", $cipher);	
	}
}

if (isset($matrnr) && isset($passwd)) {
	/* perform login to get session cookie */
	$cookieFile = tempnam(sys_get_temp_dir(), 'campus_');

	/* open database */
	$db = new SQLite3($roomDatabasePath, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);

	/* check schema */
	$result = $db->querySingle('SELECT name FROM sqlite_master WHERE type="table" AND name="rooms";');
	if (!$result) {
		$db->exec('create table rooms (id VARCHAR(255) PRIMARY KEY, address VARCHAR(255), cluster VARCHAR(255), building VARCHAR(255), building_no INTEGER, room VARCHAR(255), room_no INTEGER, floor VARCHAR(255));');
	}

	curl_request('GET', $baseUrl . $homePath, $cookieFile);

	$loginParams = array(
		'login'		=> urlencode('> Login'),
		'p'		=> urlencode($passwd),
		'u'		=> urlencode($matrnr)
	);

	curl_request('POST', $baseUrl . $loginPath, $cookieFile, $loginParams);

	/* request calendar */
	$calParams = array(
		'startdt'	=> strftime('%d.%m.%Y', time() - 7*24*60*60), /* eine Woche Vergangenheit */
		'enddt'		=> strftime('%d.%m.%Y', time() + 6*31*24*60*60) /* halbes Jahr ZUukunft */
	);

	$response = curl_request('GET', $baseUrl . $calPath, $cookieFile, $calParams);

	/* filter some changes */
	list($headers, $body) = explode("\r\n\r\n", $response, 2);

	if (substr($body, 0, strlen("BEGIN:VCALENDAR")) != "BEGIN:VCALENDAR") {
		error();
	}

	/* header pass through */
	$headers = array_slice(explode("\r\n", $headers), 1);
	foreach ($headers as $header) {
		list($key, $value) = explode(": ", $header);

		switch($key) {
			case 'Content-Disposition':
				$value = 'attachment; filename=campus_office_' . $matrnr . '.ics';
				break;
			case 'Content-Type':
				$value .= '; charset=utf-8';
			break;
		}

		if ($key != 'Content-Length') { // ignore old length
			header($key . ': ' . $value);
		}
	}
	$events = array();
	$events_output = array();
	$eventnum = 0;
	$address = array();
	$category = '';
	$lines = explode("\r\n", $body);
	$header_done = false;

	foreach ($lines as $line) {

		if (!$header_done) {
			if (substr($line,0,12) == "BEGIN:VEVENT") {
				$header_done = true;
			} else {
				echo $line . "\r\n";
			}
		}

		$line = trim($line);
		if ($line == "BEGIN:VEVENT") {
			$events[$eventnum] = array();
			$events[$eventnum]['address'] = array();

		} else if ($line == "END:VEVENT") {
			$eventnum++;

		} else if ($line) {
			list($key, $value) = explode(":", $line);
			$events[$eventnum][$key] = $value;
		}

	}

	/* Find out location */
	foreach ($events as $eventKey => $event) {
		foreach ($event as $key => $value) {
			if ($key == 'LOCATION') {
				$matches = array();
				if (preg_match('/^([0-9]+\|[a-zA-Z0-9]+)/', $value, $matches)) {
					$room = $matches[0];
					$event['address'] = get_address($db, $room);

					if (empty($event['address'])) {
						$event['address'] = crawl_address($room);
						set_address($db, $room, $event['address']);
					}
				}
				break;				
			}
		}
		$events[$eventKey] = $event;
	}

	foreach ($events as $event) {
		echo "BEGIN:VEVENT\r\n";
		foreach ($event as $key => $value) {
			if (is_array($value)) continue;
			if ($key == "END" && trim($value) == "VCALENDAR") continue;

			switch ($key) {
				case 'SUMMARY':
					$value = parseSummary($summary, $event);	
					break;

				case 'LOCATION':
					$matches = array();
					if (preg_match('/^([0-9]+\|[a-zA-Z0-9]+)/', $value, $matches)) {
						$room = $matches[0];
						
						if (empty($event['address'])) {
							$event['address'] = crawl_address($room);
							set_address($db, $room, $event['address']);
						}

						if (isset($event['address']['address'])) {
							$value = $event['address']['address'] . ', Aachen';
						}
					}
					break;

				case 'DESCRIPTION':
					$additional = $value;
					$value = '';
					$address = $event['address'];

					if (@$address['building'] || @$address['building_no']) {
						$value .= '\nGebäude: ';
						if (@$address['building_no']) {
							$value .= $address['building_no'] . ' ';
						}
						if (@$address['building']) {
							$value .= $address['building'];
						}
					}


					if (@$address['room'] || @$address['room_no']) {
						$value .= '\nRaum: ';
						if (@$address['room_no']) {
							$value .= $address['room_no'] . ' ';
						}
						if (@$address['room']) {
							$value .= $address['room'];
						}
					}

					if (@$address['floor'])
						$value .= '\nGeschoss: ' . $address['floor'];

					if (@$address['cluster'])
						$value .= '\nCampus: ' . preg_replace('/^Campus /', '', $address['cluster']);

					if (@$event['CATEGORIES'])
						$value .= '\nTyp: ' . $event['CATEGORIES'];

					if ($additional && $additional != 'Kommentar')
						$value .= '\n' . $additional;

					$value = preg_replace('/^\\\n/', '', $value);

					break;
			}
			echo $key . ':' . trim($value) . "\r\n";
		}
		echo "END:VEVENT\r\n\r\n";
	}

	echo "END:VCALENDAR\r\n";

	/* cleanup */
	unlink($cookieFile);
	$db->close();
}
else {
	echo '<?xml version="1.0" ?>';
?>

	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
		CampusOffice-Kalendar Sync

		<form action="encrypt.php" method="post">
			Matrikelnummer:<br>
 	 		<input type="text" name="matr"><br>
			Password:<br>
			<input type="password" name="pass"><br>
			<input type="checkbox" name="showCategory" value="1"> Kategorie anzeigen<br>
			<input type="checkbox" name="showRoom" value="1"> Raum anzeigen<br>
			<input type="submit">
		</form>
	</html>

	<?php
}
?>
