# cocal - CampusOffice to Google Sync
Allows to import your Campus Office Calendar into Google Calendar or any other calendar that can load iCal via HTTP(S). This way the Campus Office Calendar is automatically updated and available on all your (mobile) devices.

This fork offers improvements over the original, wich can be found on Github:
https://github.com/stv0g/snippets/blob/master/php/campus/cocal.php

This fork is based on another fork:
https://git.rwth-aachen.de/dario.wiesner/cocal

## Setup
- Create a public/private keypair compatible with PHP's OpenSSL implementation.
- Modify `cocal.php` and `encrypt.php`: Set `$privateKeyPath`, `$publicKeyPath`, and `$roomDatabasePath` according to your setup. Make sure the folder in which the database is to be placed can be modified by the executing process. (see https://stackoverflow.com/q/1485525/3764634)
- Make sure the private (and public) key isn't visible to the outside world.

## Usage
Visit `http(s)://PATH/TO/COCAL/cocal.php` in your browser. The site contains a form which you need to fill according to your needs. You receive a link to your calendar after submitting the form.

